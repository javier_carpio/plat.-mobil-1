﻿var control = false;

function dispositivo(){
  var disp = 'Tablet';

  if(/Android|iPhone|webOS/i.test(navigator.userAgent)){
    disp = 'Phone';
  }else if(/iPad/i.test(navigator.userAgent)){
    disp = 'Tablet';
  }else if(/Linux i686|Mac68K|Win16|Win32/i.test(navigator.platform)){
    disp = 'Desktop';
  }
  
  if(control == false){
    document.getElementById("disp").style.opacity="1";
    control = true;
  }else if(control == true){
    document.getElementById("disp").style.opacity="0";
    control = false;
  }

  document.getElementById("resultado").value = "The device is: " + disp;

  return disp;
}